package Models;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ManagedBean
@ViewScoped
public class User {
	
	@NotNull(message="Please enter a first name value!")
	@Size(min=2, max=30, message="First name must be between 2 and 30 char")
	String firstName = "";
	
	@NotNull(message="Please enter a first name value!")
	@Size(min=2, max=30, message="First name must be between 2 and 30 char")
	String lastName = "";
	
	public User() {

		firstName = "hermes";
		lastName = "mimini";
		
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	

}
